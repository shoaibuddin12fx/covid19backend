<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
    //
    public static function success($msg, $data = [], $code = 200){

        $msgArray = array(
            'bool' => true,
            "message" => $msg
        );

        $returnArray = array_merge($msgArray, $data);

        return response()->json( $returnArray, $code);
    }

    public static function failure($error, $data = [], $code = 422 ){

        $msgArray = array(
            'bool' => false,
            "message" => $error
        );

        $returnArray = array_merge($msgArray, $data);

        return response()->json( $returnArray, $code);

    }
}
