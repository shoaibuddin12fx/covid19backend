<?php

namespace App\Http\Controllers;
use App\User;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function checkLogin(Request $request){
        
        $data = $request->all();
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            self::failure($validator->messages());
        }

        $email = $data['email'];

        $user = User::whereNotNull('email')
            ->where('email', $email)->first();

        if($user){
            return self::success('Correct Email', ['user' => $user]);
        }else{
            return self::failure('Invalid Email');
        }

    }

    public function apiLogin(Request $request){

        $data = $request->all();
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            self::failure($validator->messages());
        }

        $email = $data['email'];

        $user = User::whereNotNull('email')
            ->where('email', $email)->first();

        if($user){

            if($user->api_key == null){
                $user->api_key = hash('sha256', Str::random(60));
                $user->save();
            }

            return self::success('User Logged In', ['user' => $user]);

        }else{



            // $resource = new UserProfileResource($user);
            // $resource['islogin'] = 1;
            $user = User::create(['email' => $email]);
            $user->api_key = hash('sha256', Str::random(60));
            $user->save();

            return self::success('User Logged In', ['user' => $user]);

        }






    }

    public function postCovidReport(Request $request){

        $user = Auth::user();

        $data = $request->all();
        $choices = $data['choices'];
        if(!$choices || $choices == []){
            return self::failure("Report Choices can't be null");
        }

        $report = [
            'user_id' => $user->id,
            'choices' => json_encode($data['choices']),
            'report' => json_encode($data['report'])
        ];

        $r = Report::create($report);

        return self::success("Record Captured, Thank you", $r->toArray());
    }

    public function downloadRecords(Request $request){

        // $user = Auth::user();
        // $table = $user->id == 1 ? Report::all() :  Report::where('user_id', $user->id)->get();
        $table = Report::all();

        $filename = "records.csv";
        $handle = fopen($filename, 'w+');

        if($table->count() == 0){
            return self::failure("No Records Found");
        }

        $head = [
            'user_id',
        ];

        $row = json_decode( $table->first()->choices, true);

        foreach ($row as $key => $value){
            $head[] = $key;
        }

        $compiled = [];

        foreach ($table as $record) {
            $eachline = $record->toArray();
            $plist = array();

            $values = json_decode($eachline["choices"], true);

            foreach ($head as $h){

                if($h == 'user_id'){
                    $plist[] = $eachline['user_id'];
                    continue;
                }

                $plist[] = implode( ' | ', $values[$h]);
            }

            $compiled[] = $plist;

        }

        return $this->fileDownload($compiled, $head);




    }

    public function fileDownload($array, $headings)
    {
        // add headings to array

        $results = $array;

        array_unshift($results, $headings);

        $headers = $this->getFileResponseHeaders('report.csv');

        return $this->streamFile(function () use ($results) {
            $output = fopen('php://output', 'w');
            foreach ($results as $row) {
                fputcsv($output, $row);
            }
            fclose($output);
        }, $headers);
    }

    protected function streamFile($callback, $headers)
    {
        $response = new StreamedResponse($callback, 200, $headers);
        $response->send();
    }

    protected function getFileResponseHeaders($filename)
    {
        return [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$filename,
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];
    }




}
